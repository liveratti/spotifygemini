# SpotifyGemini Versión 1.0

# Funcionalidades
- [x] Inicio y cierre de sesión
- [x] Busqueda por nombre de canción
- [x] Busqueda por nombre de album
- [x] Busqueda por nombre de artista
- [x] Detalles de canción
- [x] Detalles de album
- [x] Detalles de artista
- [x] Reproducción de 30 segundos de alguna canción en Busqueda
- [x] Logging de eventos con Firebase Analytics

## Requerimientos
- iOS 14.5+
- Xcode 12
- CocoaPods 1.10.1

## Instalación

### CocoaPods
Se requiere [CocoaPods](http://cocoapods.org/) para instalar:

- pod 'SDWebImage'
- pod 'Appirater'
- pod 'Firebase/Analytics'

## Arquitectura

MVVM

## Author

Abraham Rubio Zarazúa
