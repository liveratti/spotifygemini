//
//  SearchResultSubtitleTableViewCellViewModel.swift
//  Spotify
//
//  Created by Abraham Rubio on 4/21/21.
//

import Foundation


struct SearchResultSubtitleTableViewCellViewModel {
    let title: String
    let subTitle: String?
    let imageURL: URL?
}
