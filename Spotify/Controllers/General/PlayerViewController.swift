//
//  PlayerViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import UIKit
import SDWebImage


class PlayerViewController: UIViewController {
    
    weak var dataSource: PlayerDataSource?
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    private let controlsView = PlayerControlsView()
    
    private let gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.random().cgColor, UIColor.systemBackground]
        return gradient
    }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.addSublayer(gradient)
        view.backgroundColor = .systemBackground
        view.addSubview(imageView)
        view.addSubview(controlsView)
        configureBarButtons()
        
        configure()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        view.frame = CGRect(x: 0, y: 0, width: view.width, height: view.height)
        gradient.frame = view.bounds
        view.layer.insertSublayer(gradient, at: 0)
        
        let imageSize = view.width/1.2
        imageView.frame = CGRect(
            x: (view.width-imageSize)/2,
            y: view.safeAreaInsets.top+(view.width-imageSize)/2,
            width: imageSize,
            height: imageSize)
        
        let controlsViewWidth = view.width/1.1
        controlsView.frame = CGRect(
            x: (view.width-controlsViewWidth)/2,
            y: view.width+30,
            width: controlsViewWidth,
            height: controlsViewWidth-20)
        
    }
    
    private func configure() {
        imageView.sd_setImage(with: dataSource?.imageURL, completed: nil)
        controlsView.configureLabels(
            withViewModel: PlayerControlsViewViewModel(
                title: dataSource?.songName,
                subTitle: dataSource?.subTitle))
    }
    
    private func  configureBarButtons() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(didTapClose))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(didTapAction))
    }
    
    @objc private func didTapClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func didTapAction() {
        // actions
    }
    
    func refreshUI() {
        configure()
    }
}

