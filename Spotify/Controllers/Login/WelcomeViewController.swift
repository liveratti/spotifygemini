//
//  WelcomeViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import UIKit
import FirebaseAnalytics


class WelcomeViewController: UIViewController {
    
    /* botton de inicio de sesion en medio de la pantalla en la vista de WelcomeViewController*/
    private let signInButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .darkGray
        button.setTitle("Inicar Sesión en Spotify", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(25)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 25.0
        
        return button
    }()
    
    /* imagen de fondo en la vista de inicio de sesion*/
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "spotify_example_collage")
        return imageView
    }()
    
    private let overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.7
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(imageView)
        view.addSubview(overlayView)
        
        view.backgroundColor = .black
        view.addSubview(signInButton)
        signInButton.addTarget(self, action: #selector(didTapSignIn), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.frame = view.bounds
        overlayView.frame = view.bounds
        
        signInButton.frame = CGRect(
            x: 20,
            y: view.height-50-view.safeAreaInsets.bottom-350,
            width: view.width-40,
            height: 50)
    }
    
    @objc func didTapSignIn() {
        let vc = AuthViewController()
        vc.completionHandler = {[weak self] success in
            DispatchQueue.main.async {
                self?.handleSignIn(success: success)
            }
        }
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
        
        /* Evento de Analytics para registrar cuando usuario da tap en iniciar sesion*/
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
          AnalyticsParameterItemID: "Just a random id)",
          AnalyticsParameterItemName: "Just a random parmeter",
          AnalyticsParameterContentType: "cont"
          ])
        
    }
    
    private func handleSignIn(success: Bool) {
        /* Se logea usario o muestra error*/
        guard success else {
            let alert = UIAlertController(title: "Error", message: "Hubo un problema al intentar iniciar sesión", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        let mainAppTabBar = TabBarViewController()
        mainAppTabBar.modalPresentationStyle = .fullScreen
        present(mainAppTabBar, animated: true, completion: nil)
        
    }
}
