//
//  WebViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 4/21/21.
//

import UIKit
import WebKit

/*
 Class:WebViewController
 Clase del tipo view controller para abrir contenido en una webview
 */
class WebViewController: UIViewController, WKUIDelegate {
    
    var webView: WKWebView!
    
    var artistURL: URL? = nil
     
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        guard let myURL = artistURL else {
            return
        }
        let myRequest = URLRequest(url: myURL)
        webView.load(myRequest)
    }
    
    
    
}
