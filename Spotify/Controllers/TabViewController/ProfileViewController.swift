//
//  SettingsViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import UIKit

/*
 Class: SettingsViewController
 Clase de tipo ViewController para el perfil del usuario
 */
class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /* Creación de tabla para el elemento Cerrar Sesión*/
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    
    }()
    
    private var sections = [Section]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        configureModels()
        
        title = "Perfil"
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    /* Seccion del tableview para ejecutar cierre de sesion */
    private func configureModels() {        
        sections.append(Section(title: "Cuenta", options: [Option(title: "Cerrar Sesión", handler: { [ weak self] in
            DispatchQueue.main.async {
                self?.signOutTapped()
            }
        })]))
    }
    
    /* Funcion para cerrar sesión */
    private func signOutTapped() {
        let alert = UIAlertController(
            title: "Cerrar Sesión",
            message: "¿Deseas cerrar sesión?",
            preferredStyle: .alert)
        
        alert.addAction(
            UIAlertAction(
                title: "Cancelar",
                style: .cancel,
                handler: nil))
        
        
        alert.addAction(UIAlertAction(title: "Cerrar Sesión", style: .destructive, handler: { (_) in
            AuthManager.shared.signOut { [weak self] (signedOut) in
                if signedOut {
                    DispatchQueue.main.async {
                        let navVC = UINavigationController(rootViewController: WelcomeViewController())
                        navVC.navigationBar.prefersLargeTitles = true
                        navVC.viewControllers.first?.navigationItem.largeTitleDisplayMode = .always
                        navVC.modalPresentationStyle = .fullScreen
                        self?.present(navVC, animated: true, completion: {
                            self?.navigationController?.popToRootViewController(animated: false)
                        })
                    }
                }
            }
        }))
        
        present(alert, animated: true, completion: nil)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds

    }
    
    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = sections[indexPath.section].options[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = model.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // call handler for cell
        let model = sections[indexPath.section].options[indexPath.row]
        model.handler()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let model = sections[section]
        return model.title
    }
    
}


