//
//  SearchViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import UIKit
import SafariServices

class SearchViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    let searchController: UISearchController = {
        let vc = UISearchController(searchResultsController: SearchResultsViewController())
        vc.searchBar.placeholder = "Busqueda"
        vc.searchBar.searchBarStyle = .default
        vc.definesPresentationContext = true
        
        return vc
    }()
    
    private let collectionView: UICollectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout(sectionProvider: { (_, _) -> NSCollectionLayoutSection? in
            
            let item = NSCollectionLayoutItem(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .fractionalHeight(1)
                )
            )
            
            item.contentInsets = NSDirectionalEdgeInsets(top: 0,
                                                         leading: 7.5,
                                                         bottom: 0,
                                                         trailing: 7.5)
            
            
            let group = NSCollectionLayoutGroup.horizontal(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .absolute(110)
                ),
                subitem: item,
                count: 2
            )
            
            group.contentInsets = NSDirectionalEdgeInsets(top: 7.5,
                                                         leading: 10,
                                                         bottom: 7.5,
                                                         trailing: 10)
            
            return NSCollectionLayoutSection(group: group)
            
        }))
    
    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemBackground
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let resultsController = searchController.searchResultsController as? SearchResultsViewController,
              let query = searchBar.text,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else  {
            return
        }
        
        resultsController.delegate = self
        
        SpotifyAPIManager.shared.search(withQuery: query) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let results):
                    resultsController.update(withResults: results)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
    }
}

extension SearchViewController: SearchResultsViewControllerDelegate {
    
    
    func didTapResult(_ result: SearchResult) {
        switch result {
        case .album(let model):
            let vc = AlbumViewController(album: model)
            vc.navigationItem.largeTitleDisplayMode =  .never
            navigationController?.pushViewController(vc, animated: true)
        case .artist(let model):
            guard let url = URL(string: model.external_urls["spotify"] ?? "") else {
                return 
            }
            let vc = WebViewController()
            vc.artistURL = url
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.title = model.name
            navigationController?.pushViewController(vc, animated: true)
            
        case .track(let model):
            PlaybackPresenter.shared.startPlayback(fromVC: self, track: model)
        }
    }
}
