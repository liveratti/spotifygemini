//
//  TabBarViewController.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Instancia del controlador de busqueda tab y perfil tab*/
        let searchVC = SearchViewController()
        let profileVC = ProfileViewController()
        
        /* Titulos para las tabs*/
        searchVC.title = "Busqueda"
        profileVC.title = "Perfil"
        
        searchVC.navigationItem.largeTitleDisplayMode = .always
        profileVC.navigationItem.largeTitleDisplayMode = .always
        
        let searchNav = UINavigationController(rootViewController: searchVC)
        let ProfileNav = UINavigationController(rootViewController: profileVC)
        
        searchNav.navigationBar.tintColor = .label
        ProfileNav.navigationBar.tintColor = .label
        
        /* tabbar items con nombre e icono de la libreria del sistema*/
        searchNav.tabBarItem = UITabBarItem(title: "Buscar", image: UIImage(systemName: "magnifyingglass"), tag: 2)
        ProfileNav.tabBarItem = UITabBarItem(title: "Perfil", image: UIImage(systemName: "person.crop.circle"), tag: 2)
        
        searchNav.navigationBar.prefersLargeTitles = true
        ProfileNav.navigationBar.prefersLargeTitles = true
        
        setViewControllers([searchNav, ProfileNav], animated: false)
    }
    

}
