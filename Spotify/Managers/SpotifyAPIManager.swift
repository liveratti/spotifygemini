//
//  APICaller.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import Foundation

final class SpotifyAPIManager {
    static let shared = SpotifyAPIManager()
    
    private init() {}
    
    struct Constants {
        static let baseAPIUrl = "https://api.spotify.com/v1"
    }
    
    enum APIError: Error {
        case failedToGetData
    }
    
    /* Funcion para obtener los detalles de un album, fecha, nombre, canciones, etc*/
    public func getAlbumDetails(forAlbum album: Album, completion: @escaping(Result<AlbumDetailsResponse, Error>) -> Void) {
        createRequest(
            withUrl: URL(string: Constants.baseAPIUrl + "/albums/" + album.id),
            withType: .GET) { (request) in
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data, error == nil else {
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                do {
                    let result = try JSONDecoder().decode(AlbumDetailsResponse.self, from: data)
                    print(result)
                    completion(.success(result))
                } catch {
                    print(error.localizedDescription)
                    completion(.failure(error))
                }
            }
            task.resume()
        }
    }

    /* Funcion de busqueda donde recibe el texto a buscar y obtine albums, artistas y canciones*/
    public func search(withQuery query: String, completion: @escaping(Result<[SearchResult], Error>) -> Void) {
        createRequest(
            withUrl: URL(
                string: Constants.baseAPIUrl + "/search?limit=10&type=album,artist,track&q=\(query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")"), withType: .GET) { (request) in
            
            print(request.url?.absoluteString ?? "none")
            
            let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
                guard let data = data, error == nil else {
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                do {
                    let result = try JSONDecoder().decode(SearchResultResponse.self, from: data)
                    var searchResults: [SearchResult] = []
                    
                    searchResults.append(contentsOf: result.tracks.items.compactMap({ SearchResult.track(model: $0) }))
                    searchResults.append(contentsOf: result.albums.items.compactMap({ SearchResult.album(model: $0 )}))
                    searchResults.append(contentsOf: result.artists.items.compactMap({ SearchResult.artist(model: $0 )}))
                    
                    print("ResultadosBusqueda: \(searchResults)")
                    completion(.success(searchResults))
                } catch {
                    print(error)
                    completion(.failure(error))
                }
            }
            task.resume()
        }
    }
    
    
    enum HTTPMethod: String {
        case GET
        case POST
        case PUT
        case DELETE
    }
    
    /* Funcion para crear peticiones*/
    private func createRequest(withUrl url: URL?,
                               withType type: HTTPMethod,
                               completion: @escaping(URLRequest) -> Void) {
        AuthManager.shared.withValidToken { (token) in
            guard let apiURL = url else {
                return
            }
            var request = URLRequest(url: apiURL)
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            request.httpMethod = type.rawValue
            request.timeoutInterval = 30
            completion(request)
            
        }
    }
}
