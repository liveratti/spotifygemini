//
//  Playlist.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import Foundation

struct Playlist: Codable {
    let description: String
    let external_urls: [String: String]
    let href: String
    let id: String
    let images: [APIImage]
    let name: String
    let owner: Owner
    let tracks: Track
    let type: String
}

struct Owner: Codable {
    let display_name: String
    let external_urls: [String: String]
    let href: String
    let id: String
    let type: String
}

struct Track: Codable {
    let href: String
    let total: Int
}
