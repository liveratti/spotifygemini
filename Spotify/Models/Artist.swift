//
//  Artist.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import Foundation


struct Artist: Codable {
    let id: String
    let name: String
    let type: String
    let external_urls: [String: String]
    
    let images: [APIImage]?
    
}
