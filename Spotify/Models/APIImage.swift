//
//  APIImage.swift
//  Spotify
//
//  Created by Abraham Rubio on 4/16/21.
//

import Foundation


struct APIImage: Codable {
    let url: String
}
