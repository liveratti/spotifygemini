//
//  SearchResultResponse.swift
//  Spotify
//
//  Created by Abraham Rubio on 4/20/21.
//

import Foundation

struct SearchResultResponse: Codable {
    let albums: SearchAlbumResponse
    let artists: SearchArtistResponse
    let tracks: SearchTracksResponse
}

struct SearchAlbumResponse: Codable {
    let items: [Album]
}

struct SearchArtistResponse: Codable {
    let items: [Artist]
}

struct SearchTracksResponse: Codable {
    let items: [AudioTrack]
}

