//
//  SettingsModels.swift
//  Spotify
//
//  Created by Abraham Rubio on 14/06/21.
//

import Foundation

struct Section {
    let title: String
    let options: [Option]
}

struct Option {
    let title: String
    let handler: () -> Void
}
